import React, { useEffect } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Checkout from "./pages/Checkout/Checkout";
import Home from "./pages/HomePage/Home";
import Product from "./pages/productPage/Product";
import Review from "./pages/Review/Review";

export default function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Product />} />
          {/* <Route path="/" element={<Home />} /> */}
          <Route path="/product" element={<Product />} />
          <Route path="/review" element={<Review />} />
          <Route path="/checkout" element={<Checkout />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}
