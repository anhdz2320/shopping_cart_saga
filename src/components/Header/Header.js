import React from "react";
import { NavLink, useNavigate } from "react-router-dom";
import style from "./Header.module.css";
import { BsFillCartFill } from "react-icons/bs";

export default function Header() {
  const navigate = useNavigate();
  const navLinkClass = ({ isActive }) => {
    return isActive ? `nav-link ${style.activeNavItem}` : "nav-link";
  };

  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
      <button
        className="navbar-toggler d-lg-none"
        type="button"
        data-toggle="collapse"
        data-target="#collapsibleNavId"
        aria-controls="collapsibleNavId"
        aria-expanded="false"
        aria-label="Toggle navigation"
      />
      <div
        className="collapse navbar-collapse justify-content-between "
        id="collapsibleNavId"
      >
        <ul className="navbar-nav mt-2 mt-lg-0">
          <li className="nav-item">
            <NavLink className={navLinkClass} to="/">
              Home{" "}
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className={navLinkClass} to="/product">
              Products
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className={navLinkClass} to="/review">
              Reviews
            </NavLink>
          </li>
        </ul>
        <div className="text-light mr-5 display-5">Beauty Shop</div>
        <NavLink to="/checkout" className="bg-transparent border-0">
          <BsFillCartFill style={{ color: "white", fontSize: 30 }} />
        </NavLink>
      </div>
    </nav>
  );
}
