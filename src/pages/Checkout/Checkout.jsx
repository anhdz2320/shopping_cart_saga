import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addTocart, deleteCart } from "../../redux/ducks/cartSlice";
import style from "./CheckoutStyle.module.css";
export default function Checkout() {
  const dispatch = useDispatch();
  const cartList = useSelector((state) => state.cart);
  const handleIncrease = (product) => {
    const foughtIndex = cartList.findIndex(
      (item) => item.productId === product.productId
    );
    let cartListClone = [...cartList];
    cartListClone[foughtIndex] = {
      ...cartListClone[foughtIndex],
      quantity: product.quantity + 1,
    };
    console.log(cartListClone, "clonecart");
    dispatch(addTocart(cartListClone[foughtIndex]));
  };

  const handleDecrease = (product) => {
    const foughtIndex = cartList.findIndex(
      (item) => item.productId === product.productId
    );
    let cartListClone = [...cartList];
    if (product.quantity === 1) {
      dispatch(deleteCart(foughtIndex));
    } else {
      cartListClone[foughtIndex] = {
        ...cartListClone[foughtIndex],
        quantity: product.quantity - 1,
      };
      console.log(cartListClone, "clonecart");
      dispatch(addTocart(cartListClone[foughtIndex]));
    }
  };
  const caclSubTotal = () => {
    if (cartList.legth === 0) return 0;
    return cartList.reduce(
      (total, item) => total + item.quantity * item.price,
      0
    );
  };
  const caclShipCost = () => {
    if (!cartList.length) return 0;

    return 10;
  };
  return (
    <div>
      <h3 className="text-center text-info">My Shopping Cart</h3>
      <div className="cart_content d-flex mr-0">
        <div className="cart_list w-50">
          {cartList.map((item, index) => (
            <div key={index} className={style.cart_item}>
              <img src={item.imageUrl} alt="img_product" />
              <div className="detail">
                <p>{item.productName}</p>
                <p>{item.description}</p>
                <div className="d-flex">
                  <div className="quantity">
                    <button onClick={() => handleDecrease(item)}>-</button>
                    <button>{item.quantity}</button>
                    <button
                      onClick={() => {
                        handleIncrease(item);
                      }}
                    >
                      +
                    </button>
                  </div>
                  <span className="price ml-5">
                    ${item.quantity * item.price}
                  </span>
                </div>
              </div>
            </div>
          ))}
        </div>
        <div className="order">
          <div className="order_info">
            <h4>Order Info</h4>
            <p>
              <span className="mr-5">Subtotal</span>
              <span>${caclSubTotal()}</span>
            </p>
            <p>
              <span className="mr-5">Shipping cost</span>
              <span>${!cartList.length ? 0 : 10}</span>
            </p>
            <p>
              <span className="mr-5">Total</span>
              <span>${caclSubTotal() + caclShipCost()}</span>
            </p>
          </div>
          <div className="checkout"></div>
        </div>
      </div>
    </div>
  );
}
