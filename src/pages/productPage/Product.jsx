import React, { useEffect, useState } from "react";
import toast, { Toaster } from "react-hot-toast";
import { useDispatch, useSelector } from "react-redux";
import Header from "../../components/Header/Header";
import { addTocart } from "../../redux/ducks/cartSlice";
import { getProduct } from "../../redux/ducks/productSlice";
import style from "./ProductStyle.module.css";
export default function Product() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProduct());
  }, [dispatch]);
  const productList = useSelector((state) => state.product);

  const [detailProduct, setDetailProduct] = useState(null);
  const renderDetail = () => {
    if (!detailProduct) {
      setDetailProduct({...productList[0],quantity:1});
    }

    const handleDecrease = () => {
      if (detailProduct.quantity === 1) return;
      else
        setDetailProduct({
          ...detailProduct,
          quantity: detailProduct.quantity - 1,
        });
    };
    const handleIncrease = () => {
      setDetailProduct({
        ...detailProduct,
        quantity: detailProduct.quantity + 1,
      });
    };
    return (
      <div className={style.product_detail}>
        <div className="info">
          <div className="img_product">
            <img src={detailProduct?.imageUrl} alt="img" />
          </div>
          <p>{detailProduct?.productName}</p>
          <p>{detailProduct?.description}</p>
        </div>
        <div className="action">
          <div className="quantity">
            <button onClick={handleDecrease}>-</button>
            <span>{detailProduct?.quantity}</span>
            <button onClick={handleIncrease}>+</button>
          </div>
          <div className="price">
            ${detailProduct?.quantity * detailProduct?.price}
          </div>
          <button
            onClick={() => {
              toast.success("Add to cart successfully!")
              dispatch(addTocart(detailProduct))
            }}
            className="btn btn-success"
          >
            Add to cart
          </button>
          <Toaster />
        </div>
      </div>
    );
  };
  return (
    <div>
      <Header />
      {productList.length > 0 ? (
        <div className="body d-flex p-4">
          {renderDetail()}
          <div className={style.product_list}>
            {productList.map((item, index) => (
              <div className="content_item d-flex" key={index}>
                <img
                  style={{ width: "30%" }}
                  src={item.imageUrl}
                  alt="img_product"
                />
                <div className="detail">
                  <p>{item.productName}</p>
                  <p>{item.description}</p>
                  <p>
                    <span>${item.price}</span>
                    <button
                      className="btn btn-info"
                      onClick={() => {
                        setDetailProduct({ ...item, quantity: 1 });
                      }}
                    >
                      Detail
                    </button>
                  </p>
                </div>
              </div>
            ))}
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}
