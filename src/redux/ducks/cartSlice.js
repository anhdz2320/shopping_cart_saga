import { createSlice } from "@reduxjs/toolkit";

const initialState = [];

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addTocart: (state, action) => {
      // state.push(action.payload);
      let foughtIndex = state.findIndex(
        (item) => item.productId === action.payload.productId
      );
      if (foughtIndex === -1) {
        state.push(action.payload);
      } else {
        state[foughtIndex] = {
          ...state[foughtIndex],
          quantity: action.payload.quantity,
        };
      }
    },
    deleteCart: (state, action) => {
      state.splice(action.payload, 1);
    },
  },
});

// Action creators are generated for each case reducer function
export const { addTocart, deleteCart } = cartSlice.actions;

export default cartSlice.reducer;
