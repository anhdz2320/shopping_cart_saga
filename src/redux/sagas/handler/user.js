import { call, delay, put } from "redux-saga/effects";
import { setProduct } from "../../ducks/productSlice";
import { requestGetProduct } from "../request/product";

export function* handleGetProduct(action) {
  try {
    const response = yield call(requestGetProduct);
    // yield delay(300);
    const { data } = response;
    yield put(setProduct(data));
  } catch (error) {
    console.log(error);
  }
}
